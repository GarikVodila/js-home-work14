const pass = document.querySelector(".password");
const confPass = document.querySelector(".password-confirm");
const iconPass = document.querySelector(".icon-password-one");
const iconConfPass = document.querySelector(".icon-password-two");
const attention = document.querySelector(".attention");

iconPass.onclick = showPass;
iconConfPass.onclick = showPassConf;

function showPass () {
    if (pass.type == 'password') {
        pass.type = 'text';
        toggleIcon(this, false);
    } else {
        pass.type = 'password';
        toggleIcon(this, true);
    }
}

function showPassConf () {
    if (confPass.type == 'password') {
        confPass.type = 'text';
        toggleIcon(this, false);
    } else {
        confPass.type = 'password';
        toggleIcon(this, true);
   };
}

function toggleIcon(icon, isShow) {
    if (isShow) {
        icon.classList.add('fa-eye');
        icon.classList.remove('fa-eye-slash');
    } else {
        icon.classList.remove('fa-eye');
        icon.classList.add('fa-eye-slash');
    };
}
   
function checkPass() {
    if (pass.value !== "" && confPass !== "") {
        if (pass.value === confPass.value){
            alert('You are welcome');
        } else {
            errorOutput('Потрібно ввести однакові значення');
        };
    };
}

const btn = document.querySelector(".btn")
btn.onclick = checkPass;


////////////смена темы/////////

const passwordForm =document.querySelector(".password-form")
const button = document.querySelector(".change-color");

const currentTheme = localStorage.getItem("theme");


if (currentTheme == "new") {
    document.body.classList.add("new-theme");
    button.classList.add("change-color-new-theme");
    passwordForm.classList.add("password-form-new-theme");
    btn.classList.add("btn-new-theme");
  }
  
button.addEventListener("click", function () {
      
    document.body.classList.toggle("new-theme");
    button.classList.toggle("change-color-new-theme");
    passwordForm.classList.toggle("password-form-new-theme");
    btn.classList.toggle("btn-new-theme");
    let theme = "default";
  
    if (
        document.body.classList.contains("new-theme") &&
        button.classList.contains("change-color-new-theme") &&
        passwordForm.classList.contains("password-form-new-theme") &&
        btn.classList.contains("btn-new-theme")
    ) {
      theme = "new";
    }
  
    localStorage.setItem("theme", theme);
  });
 
